package cz.cuni.lf1.lge.thunderstorm.util.macroui.sample;

import cz.cuni.lf1.lge.thunderstorm.util.macroui.ParameterKey;
import cz.cuni.lf1.lge.thunderstorm.util.macroui.ParameterTracker;
import cz.cuni.lf1.lge.thunderstorm.util.macroui.DialogStub;
import cz.cuni.lf1.lge.thunderstorm.util.macroui.validators.DoubleValidatorFactory;
import cz.cuni.lf1.lge.thunderstorm.util.macroui.validators.StringValidatorFactory;
import ij.IJ;
import ij.Macro;
import ij.plugin.PlugIn;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
public class SamplePlugin implements PlugIn {

    String tab1Title = "tab1";
    String tab2Title = "tab2";
    ParameterTracker params = new ParameterTracker("SamplePlugin.");
    ParameterKey.String tab = params.createStringField("tab", null, tab1Title);
    //parameter image will be processed only if "tab1" is selected
    ParameterKey.String image = params.createStringField("img", StringValidatorFactory.openImages(true), "", new ParameterTracker.Condition() {
        public boolean isSatisfied() {
            return tab2Title.equals(tab.getValue());
        }

        public ParameterKey[] dependsOn() {
            return new ParameterKey[]{tab};
        }
    });
    //parameter "number" will be processed only if "tab2" is selected
    ParameterKey.Double number = params.createDoubleField("number", DoubleValidatorFactory.rangeInclusive(0, 1), 0.5, new ParameterTracker.Condition() {
        public boolean isSatisfied() {
            return tab1Title.equals(tab.getValue());
        }

        public ParameterKey[] dependsOn() {
            return new ParameterKey[]{tab};
        }
    });

    public void run(String arg) {
        if (Macro.getOptions() != null) {
            params.readMacroOptions();
        } else {
            DialogStub dialog = new DialogStub(params, null, "Sample dialog") {
                @Override
                protected void layoutComponents() {
                    getRootPane().setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                    setLayout(new BorderLayout(0, 5));
                    JTabbedPane tabsPane = new JTabbedPane();
                    tab.registerComponent(tabsPane);
                    JPanel tab1Panel = new JPanel(new GridBagLayout());
                    JPanel tab2Panel = new JPanel(new GridBagLayout());

                    tabsPane.add(tab1Title, tab1Panel);
                    tabsPane.add(tab2Title, tab2Panel);

                    //fill first tab                   
                    tab1Panel.add(new JLabel("Input number between 0 and 1:"), firstColumn(0));
                    tab1Panel.add(number.registerComponent(new JTextField(10)), secondColumn(0));
                    tab1Panel.add(new JLabel("[units]"), thirdColumn(0));
                    //fill second tab
                    tab2Panel.add(new JLabel("Choose open image:"), firstColumn(0));
                    tab2Panel.add(image.registerComponent(createOpenImagesComboBox(true)), secondColumn(0));
                    tab2Panel.add(Box.createGlue(), thirdColumn(0));
                    
                    params.loadPrefs();

                    add(tabsPane, BorderLayout.CENTER);
                    add(createButtonsPanel(), BorderLayout.SOUTH);
                    setResizable(true);
                    pack();
                    setLocationRelativeTo(null);
                }
            };
            dialog.setModal(false);

            if (dialog.showAndGetResult() != JOptionPane.OK_OPTION) {//blocks untill the dialog is closed
                return;
            }
        }

        if (tab1Title.equals(tab.getValue())) {
            IJ.showMessage("tab = " + tab.getValue() + "\np1 = " + number.getValue());
        } else {
            IJ.showMessage("tab = " + tab.getValue() + "\nimg = " + image.getValue());
        }
    }

}
