
package cz.cuni.lf1.lge.thunderstorm.util.macroui.sample;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Josef Borkovec <josef.borkovec[at]lf1.cuni.cz>
 */
public class SamplePluginTest {
    

    @Test
    public void testRun() throws InterruptedException {
        //just to run the plugin without having to run ImageJ
        SamplePlugin sp = new SamplePlugin();
        sp.run(null);
        Thread.sleep(30000);
    }
    
}
